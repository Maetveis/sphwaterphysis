# SPH Water Physics
Smoothed Particle Hydrodinamics (SPH) based 3D water simulation running on OpenGL compute shaders

## Demo Video
![Fluid Simulation Demo](assets/demo.m4v)

## Building
### Requirements:
  * SLD2
  * SDL2_image
  * SDL2_ttf
  * glew
  * glm
  * assimp

Use make to build
